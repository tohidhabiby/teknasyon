<?php

namespace Tests;

use App\Interfaces\Models\UserInterface;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CreatesApplication;

    /**
     * @param null $name Name.
     * @param array $data Data.
     * @param string $dataName Data name.
     */
    public function __construct($name = null, $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->withHeaders([
		   'Accept' => 'application/json'
		]);
	}


    /**
     * @param User|null $loggedInUser Logged In User.
     *
     * @return User
     */
    public function actingAsUser(User $loggedInUser = null): UserInterface
    {
        if(empty($loggedInUser)) {
            $loggedInUser = User::factory()->create();
        }

        Passport::actingAs($loggedInUser);

        return $loggedInUser;
    }
}
