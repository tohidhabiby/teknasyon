<?php

namespace Database\Seeders;

use App\Jobs\SplitterJob;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        dispatch(new SplitterJob())->delay(now()->addDay())->onQueue('splitter');

        DB::table('settings')->updateOrInsert(
            [
                'key' => 'callback_endpoint'
            ],
            [
                'value' => 'http://callback.local'
            ]
        );
    }
}
