<?php

namespace Database\Seeders;

use App\Models\Device;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deviceCount = Device::count();
        $subscriptions = [];
        // make all subscriptions expired
        for ($i = 1; $i <= $deviceCount; $i++) {
            $subscriptions [] = [
                Subscription::DEVICE_ID => $i,
                Subscription::RECEIPT => Str::random(20),
                Subscription::EXPIRED_AT => Carbon::now()->subDays(10),
                Subscription::STATUS => Subscription::ACTIVE,
            ];
        }
        $chunkedData = array_chunk($subscriptions, round($deviceCount / 100));
        foreach ($chunkedData as $chunk) {
            Subscription::insert($chunk);
        }
    }
}
