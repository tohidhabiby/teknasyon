<?php

namespace Database\Factories;

use App\Models\Device;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DeviceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Device::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $uid = $this->faker->uuid;
        return [
            Device::UID => $uid,
            Device::APP_ID => $this->faker->randomNumber(6, true),
            Device::LANGUAGE => $this->faker->languageCode,
            Device::OS => $this->faker->randomElement(Device::$osTypes),
            Device::USERNAME => $this->faker->userName,
            Device::PASSWORD => encrypt(123456),
            Device::CLIENT_TOKEN => md5($uid . Str::random(20))
        ];
    }
}
