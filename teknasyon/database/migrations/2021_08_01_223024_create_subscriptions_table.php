<?php

use App\Models\Subscription;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Subscription::TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId(Subscription::DEVICE_ID)->unique()->constrained(); // make sure each device on each os has only one subscription
            $table->string(Subscription::RECEIPT);
            $table->enum(Subscription::STATUS, Subscription::$statuses)
                ->comment(implode(', ', Subscription::$statuses))->default(Subscription::INIT);
            $table->timestamp(Subscription::EXPIRED_AT)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Subscription::TABLE);
    }
}
