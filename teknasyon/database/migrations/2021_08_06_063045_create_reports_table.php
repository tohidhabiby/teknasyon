<?php

use App\Models\Report;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Report::TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId(Report::SUBSCRIPTION_ID)->constrained();
            $table->enum(Report::EVENT, Report::$events);
            $table->timestamp(Report::EVENT_DATE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Report::TABLE);
    }
}
