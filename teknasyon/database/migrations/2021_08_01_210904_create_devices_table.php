<?php

use App\Models\Device;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Device::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(Device::UID);
            $table->string(Device::APP_ID);
            $table->string(Device::LANGUAGE);
            $table->enum(Device::OS, Device::$osTypes)->comment(implode(', ', Device::$osTypes));
            $table->string(Device::USERNAME);
            $table->string(Device::PASSWORD);
            $table->string(Device::CLIENT_TOKEN)->unique();
            $table->timestamps();

            $table->unique([Device::UID, Device::APP_ID, Device::OS, Device::USERNAME]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Device::TABLE);
    }
}
