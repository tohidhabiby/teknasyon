<?php

namespace App\Jobs;

use App\Models\Report;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReportJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public Subscription $subscription;
    public Carbon $date;
    public string $event;

    /**
     * Create a new job instance.
     *
     * @param Subscription $subscription
     * @param Carbon $date
     * @param string $event
     */
    public function __construct(Subscription $subscription, Carbon $date, string $event)
    {
        $this->subscription = $subscription;
        $this->date = $date;
        $this->event = $event;
        $this->queue = 'report';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Report::create([
            Report::SUBSCRIPTION_ID => $this->subscription->getId(),
            Report::EVENT => $this->event,
            Report::EVENT_DATE => $this->date
        ]);
    }
}
