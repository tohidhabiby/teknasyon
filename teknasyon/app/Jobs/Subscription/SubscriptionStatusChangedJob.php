<?php

namespace App\Jobs\Subscription;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 *  for section CALLBACK [BASIC]
 *
 * Class SubscriptionStatusChangedJob
 * @package App\Jobs\Subscription
 */
class SubscriptionStatusChangedJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var string
     */
    private string $deviceID;

    /**
     * @var string
     */
    private string $appID;

    /**
     * @var string
     */
    private string $event;

    /**
     * Create a new job instance.
     *
     * @param string $deviceID
     * @param string $appID
     */
    public function __construct(string $deviceID, string $appID)
    {
        $this->deviceID = $deviceID;
        $this->appID = $appID;

        $this->queue = 'callback';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // todo:send data to third party end point
    }

    public function setEvent($event)
    {
        $this->event = $event;
    }
}
