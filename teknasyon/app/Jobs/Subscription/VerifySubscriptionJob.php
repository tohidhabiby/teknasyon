<?php

namespace App\Jobs\Subscription;

use App\Interfaces\Models\DeviceInterface;
use App\Jobs\ReportJob;
use App\Models\Subscription;
use App\Traits\ExternalServiceMockTrait;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class VerifySubscriptionJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use ExternalServiceMockTrait;

    /**
     * @var DeviceInterface
     */
    public DeviceInterface $device;

    /**
     * @var string
     */
    public string $receipt;

    /**
     * Create a new job instance.
     *
     * @param DeviceInterface $device Device.
     * @param string $receipt
     */
    public function __construct(DeviceInterface $device, string $receipt)
    {
        $this->device = $device;
        $this->receipt = $receipt;
        $this->queue = 'purchase';
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        // todo: call event
        // job to send subscription info to third-party endpoint
        $nextJob = new SubscriptionStatusChangedJob($this->device->getUid(), $this->device->getAppId());
        $subscription = $this->device->subscription;
        $reportJob = null;
        $isNew = false;

        // if subscription is not in init step and receipt in db is equal to request receipt
        // means we already have confirmed or cancel is receipt.
        if (
            !empty($subscription) &&
            (
                optional($subscription)->getStatus() !== Subscription::INIT &&
                $subscription->getReceipt() == $this->receipt
            )
        ) {
            return;
        }
        //todo:create separate function

        // send request to apple or google
        $response = $this->verifySubscription(
            $this->device->getOs(),
            $this->receipt,
            [$this->device->getUsername(), $this->device->getPassword()]
        );

        // limit error
        if ($response === false) {
            throw new \Exception('error');
        }

        if (empty($subscription)){
            $subscription = $this->device->subscription()->create([
                'receipt' => $this->receipt
            ]);
            $isNew = true;
        }

        if (Carbon::parse($response[Subscription::EXPIRED_AT])->gt(Carbon::now())) {
            $nextJob->setEvent($isNew ? 'started' : 'renewed');
            $response[Subscription::STATUS] = Subscription::ACTIVE;
            $reportJob = new ReportJob($subscription, now(), $isNew ? 'new' : 'renewed');
        } else {
            $nextJob->setEvent('canceled');
            $response[Subscription::STATUS] = Subscription::CANCELLED;
            $reportJob = new ReportJob($subscription, now(), 'expired');
        }
        // update subscription
        $subscription->update($response);
        // call job to report subscription status
        dispatch($nextJob);
        // call report job
        dispatch($reportJob);
    }

    public function fail($exception = null)
    {
        // if job failed retry again.
        dispatch(new VerifySubscriptionJob($this->device, $this->receipt));
    }
}
