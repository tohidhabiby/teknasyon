<?php

namespace App\Jobs;

use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SplitterJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        // splitter must be on different queue so can have specific worker
        $this->queue = 'splitter';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // count subscriptions that have been expired but status is not cancelled.
        $count = Subscription::query()->needResolve()->count();

        // each job can handle 20,000 (maximum) subscription records.
        // (we can change this number to desired value base on number of workers)
        $chunk = 20000;
        $from = 0;
        $jobs = [];
        while ($count > 0) {
            $data = Subscription::query()->select(Subscription::ID)->offset($from)->take($chunk)->needResolve()
                ->get()->pluck(Subscription::ID);
            // to prevent any unwanted results is best to run jobs after splitter work is done.
            $jobs [] = new ResolveSubscriptionJob([$data->min(), $data->max()], $data->count());

            $count -= $chunk;
            $from = $from + $chunk;
        }

        // dispatch jobs
        foreach ($jobs as $job){
            dispatch($job);
        }

        // after splitter job is done must put it self in queue to be called 24 hours later.
        dispatch(new SplitterJob())->delay(now()->addDay());
    }

    /**
     * @param null $exception
     */
    public function fail($exception = null)
    {
        dispatch(new SplitterJob())->delay(now()->addDay());
    }
}
