<?php

namespace App\Jobs;

use App\Models\Subscription;
use App\Traits\ExternalServiceMockTrait;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ResolveSubscriptionJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use ExternalServiceMockTrait;

    /**
     * @var array
     */
    public array $between;

    /**
     * @var int
     */
    public int $total;

    /**
     * @var bool
     */
    public bool $limitError = false;

    /**
     * @var bool
     */
    public bool $error = false;

    /**
     * Create a new job instance.
     *
     * @param array $between Between.
     * @param integer $total Total.
     */
    public function __construct(array $between, int $total)
    {
        $this->between = $between;
        $this->total = $total;

        $this->queue = 'resolver';
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $recordChunk = 100;
        $remain = $this->total;
        // since data in database for each job can be up to 20,000 we need to handle them peace by peace
        while ($remain >= 0) {
            $subscriptions = Subscription::query()
                ->whereBetween(Subscription::ID, $this->between)
                ->needResolve()
                ->with('device')
                ->take($recordChunk)
                ->get();

            if (empty($subscriptions)) {
                break;
            }

            $this->resolve($subscriptions);

            $remain -= $recordChunk;
        }

        if ($this->error || $this->limitError) {
            $this->fail();
        }
    }

    public function fail($exception = null)
    {
        // in case if for some reason job failed
        dispatch(new ResolveSubscriptionJob($this->between, $this->total));
    }

    /**
     * @param Collection $subscriptions Subscription.
     *
     * @throws \Exception
     */
    private function resolve(Collection $subscriptions): void
    {
        $renewed = [];
        $canceled = [];
        $reportJobs = [];

        foreach ($subscriptions as $subscription) {
            $response = $this->verifySubscription(
                $subscription->device->getOs(),
                $subscription->getReceipt(),
                [$subscription->getUsername(), $subscription->getPassword()]
            );

            if ($response === false) {
                $this->limitError = true;
                continue;
            }
            $isCanceled = Carbon::parse($response[Subscription::EXPIRED_AT])->gt(Carbon::now());

            $item = [
                Subscription::ID => $subscription->getId(),
                Subscription::STATUS => $isCanceled ? Subscription::CANCELLED : Subscription::ACTIVE,
                Subscription::EXPIRED_AT => Carbon::parse($response[Subscription::EXPIRED_AT])
            ];

            if ($isCanceled) {
                $canceled [] = $item;
                $reportJobs [] = new ReportJob($subscription, now(), 'expired');
                continue;
            }
            $renewed [] = $item;
            $reportJobs [] = new ReportJob($subscription, now(), 'renewed');
        }

        if (!empty($renewed)) {
            $response = $this->updateBatch(Subscription::TABLE, $renewed);
            $this->error = !$this->error ? $response : $this->error;
        }

        if (!empty($canceled)) {
            $response = $this->updateBatch(Subscription::TABLE, $canceled);
            $this->error = !$this->error ? $response : $this->error;
        }

        if (!empty($reportJobs)){
            foreach ($reportJobs as $job){
                dispatch($job);
            }
        }
    }

    /**
     * batch update for faster resolving
     *
     * @param string $tableName   Table name.
     * @param array $multipleData Data.
     *
     * @return mixed
     */
    public function updateBatch(string $tableName = "", array $multipleData = []): mixed
    {
        // column or fields to update
        $updateColumn = array_keys($multipleData[0]);
        $referenceColumn = $updateColumn[0];
        unset($updateColumn[0]);
        $whereIn = "";

        $q = "UPDATE " . $tableName . " SET ";
        foreach ($updateColumn as $uColumn) {
            $q .= $uColumn . " = CASE ";
            foreach ($multipleData as $data) {
                $q .= "WHEN " . $referenceColumn . " = " . $data[$referenceColumn] . " THEN '" . $data[$uColumn] . "' ";
            }
            $q .= "ELSE " . $uColumn . " END, ";
        }
        foreach ($multipleData as $data) {
            $whereIn .= "'" . $data[$referenceColumn] . "', ";
        }
        $q = rtrim($q, ", ") . " WHERE " . $referenceColumn . " IN (" . rtrim($whereIn, ', ') . ")";

        return DB::update(DB::raw($q));
    }
}
