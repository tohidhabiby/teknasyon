<?php

namespace App\Models;

use App\Interfaces\Models\UserInterface;
use App\Traits\HasEmailTrait;
use App\Traits\HasFirstNameTrait;
use App\Traits\HasIdTrait;
use App\Traits\HasLastNameTrait;
use App\Traits\HasPasswordTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements UserInterface
{
    use HasFactory;
    use Notifiable;
    use HasEmailTrait;
    use HasPasswordTrait;
    use HasFirstNameTrait;
    use HasLastNameTrait;
    use HasIdTrait;
    use MagicMethodsTrait;

    /**
     * @param string $email     Email.
     * @param string $firstName Firstname.
     * @param string $lastName  Lastname.
     * @param string $password  Password.
     *
     * @return UserInterface
     */
    public static function createObject(
        string $email,
        string $firstName,
        string $lastName,
        string $password
    ): UserInterface {
        $user = new self();
        $user->setEmail($email);
        $user->setPassword(Hash::make($password));
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->save();

        return $user;
    }
}
