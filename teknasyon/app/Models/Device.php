<?php

namespace App\Models;

use App\Interfaces\Models\DeviceInterface;
use App\Traits\HasUsernameTrait;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Device extends BaseModel implements DeviceInterface
{
    use HasUsernameTrait;

    const UID = 'uid';
    const APP_ID = 'app_id';
    const LANGUAGE = 'language';
    const OS = 'os';
    const PASSWORD = 'password';
    const CLIENT_TOKEN = 'client_token';
    const IOS = 'IOS';
    const ANDROID = 'ANDROID';

    /**
     * @var array
     */
    protected $fillable = [
        self::UID,
        self::APP_ID,
        self::LANGUAGE,
        self::OS,
        self::PASSWORD,
        self::CLIENT_TOKEN,
        self::USERNAME,
    ];

    /**
     * @var array|string[]
     */
    public static array $osTypes = [
        self::IOS,
        self::ANDROID,
    ];

    /**
     * @return HasOne
     */
    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class);
    }
}
