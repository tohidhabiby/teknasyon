<?php

namespace App\Models;

class Report extends BaseModel
{
    const TABLE = 'reports';
    const SUBSCRIPTION_ID = 'subscription_id';
    const EVENT = 'event';
    const EVENT_DATE = 'event_date';
    const EVENT_NEW = 'new';
    const EVENT_RENEW = 'renewed';
    const EVENT_EXPIRED = 'expired';

    /**
     * @var array|string[]
     */
    public static array $events = [
        self::EVENT_NEW,
        self::EVENT_RENEW,
        self::EVENT_EXPIRED,
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        self::SUBSCRIPTION_ID,
        self::EVENT,
        self::EVENT_DATE,
    ];
}
