<?php

namespace App\Models;

use App\Interfaces\Models\SubscriptionInterface;
use App\Traits\HasDeviceIdTrait;
use App\Traits\HasStatusTrait;
use Illuminate\Database\Eloquent\Builder;

class Subscription extends BaseModel implements SubscriptionInterface
{
    use HasDeviceIdTrait;
    use HasStatusTrait;

    const EXPIRED_AT = 'expired_at';
    const INIT = 'INIT';
    const ACTIVE = 'ACTIVE';
    const CANCELLED = 'CANCELLED';
    const RECEIPT = 'receipt';

    /**
     * @var string[]
     */
    protected $fillable = [
        self::DEVICE_ID,
        self::STATUS,
        self::RECEIPT,
        self::EXPIRED_AT,
    ];

    /**
     * @var array|string[]
     */
    public static array $statuses = [
        self::INIT,
        self::ACTIVE,
        self::CANCELLED,
    ];

    /**
     * subscription that have been expired but to on "canceled" status need resolving.
     *
     * @param $query
     *
     * @return Builder
     */
    public function scopeNeedResolve($query): Builder
    {
        return $query
            ->whereStatus(self::ACTIVE)
            ->where(self::EXPIRED_AT, '<', now());
    }
}
