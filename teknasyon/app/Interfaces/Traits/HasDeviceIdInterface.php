<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasDeviceIdInterface
{
    const DEVICE_ID = 'device_id';

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo;
}
