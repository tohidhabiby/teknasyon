<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasIpInterface
{
    const IP = 'ip';

    /**
     * @param Builder     $builder
     * @param string|null $ip
     * @return Builder
     */
    public function scopeWhereIpLike(Builder $builder, ?string $ip = null): Builder;
}
