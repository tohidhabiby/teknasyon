<?php

namespace App\Interfaces\Traits;

interface HasPasswordInterface
{
    const PASSWORD = 'password';

    /**
     * @param $password
     * @return boolean
     */
    public function updatePassword(string $password): bool;
}
