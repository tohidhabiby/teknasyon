<?php

namespace App\Interfaces\Models;

use App\Interfaces\Traits\HasDeviceIdInterface;
use App\Interfaces\Traits\HasStatusInterface;

interface SubscriptionInterface extends BaseModelInterface,
    HasDeviceIdInterface,
    HasStatusInterface
{
    const TABLE = 'subscriptions';
}
