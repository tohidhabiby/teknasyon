<?php

namespace App\Interfaces\Models;

use App\Interfaces\Traits\HasUsernameInterface;
use Illuminate\Database\Eloquent\Relations\HasOne;

interface DeviceInterface extends HasUsernameInterface
{
    const TABLE = 'devices';

    /**
     * @return HasOne
     */
    public function subscription(): HasOne;
}
