<?php

namespace App\Interfaces\Models;

use App\Interfaces\Traits\HasEmailInterface;
use App\Interfaces\Traits\HasFirstNameInterface;
use App\Interfaces\Traits\HasLastNameInterface;
use App\Interfaces\Traits\HasPasswordInterface;

interface UserInterface extends
    HasEmailInterface,
    HasFirstNameInterface,
    HasLastNameInterface,
    HasPasswordInterface
{
    const TABLE = 'users';
}
