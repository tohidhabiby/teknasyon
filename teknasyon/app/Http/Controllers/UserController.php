<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Resources\DeviceResource;
use App\Http\Resources\UserResource;
use App\Interfaces\Models\UserInterface;
use App\Models\Device;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{



    /**
     * @param UserInterface $user          User.
     * @param string|null   $firebaseToken Firebase Token.
     *
     * @return JsonResponse
     */
    protected function getTokenResponse(UserInterface $user, ?string $firebaseToken = null): JsonResponse
    {
        try {
            DB::beginTransaction();
            $token = $user->createToken('new user')->accessToken;
            $response = [
                'token' => $token,
                'user' => new UserResource($user),
            ];

            return $this->getResponse($response);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->getResponse(
                ['message' => __('error.token_not_generated')],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
