<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\DeviceResource;
use App\Models\Device;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * @param RegisterRequest $request Request.
     *
     * @return DeviceResource
     */
    public function register(RegisterRequest $request): DeviceResource
    {
        // todo: should we add error handler?
        $data = $request->validated();
        $data[Device::PASSWORD] = encrypt($data[Device::PASSWORD]);
        $data[Device::CLIENT_TOKEN] = md5($data[Device::UID] . ":" . (Str::random(60)));
        $device = Device::create($data);

        return new DeviceResource($device);
    }

    /**
     * @param LoginRequest $request Request.
     *
     * @return JsonResponse|DeviceResource
     */
    public function login(LoginRequest $request): JsonResponse|DeviceResource
    {
        $device = Device::whereUsername($request->get(Device::USERNAME))->first();

        if ($device) {
            if (Hash::check($request->get(Device::PASSWORD), $device->getPassword())) {
                $device->setClientToken(md5($device->getUid() . ":" . (Str::random(60))));
                $device->save();

                return new DeviceResource($device);
            }
        }

        return $this->getResponse(
            ['message' => __('error.incorrect_user_pass')],
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request Request.
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $token = $request->user()->token();
        $token->revoke();

        return $this->getResponse();
    }
}
