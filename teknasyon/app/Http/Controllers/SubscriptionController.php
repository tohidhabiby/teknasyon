<?php

namespace App\Http\Controllers;

use App\Http\Requests\Subscription\PurchaseVerificationRequest;
use App\Jobs\Subscription\VerifySubscriptionJob;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class SubscriptionController extends Controller
{
    /**
     * @param PurchaseVerificationRequest $request Request.
     *
     * @return Application|ResponseFactory|Response
     */
    public function purchase(PurchaseVerificationRequest $request): Application|ResponseFactory|Response
    {
        // since is mentioned there many requests we run job by calling event.
        //todo:make event
        $device = app()->get('device');
        // for working faster we are put this this job in different queue
        $this->dispatch(new VerifySubscriptionJob($device, $request->get('receipt')));

        return response(
            [
                'message' => 'your request successfully received and being processed.',
            ]
        );
    }

    /**
     * @return Response
     */
    public function status(): Response
    {
        $device = app()->get('device');
        $status = optional($device->subscription)->status;

        return !empty($status) ?
            response(['status' => $status]) :
            response(['message' => 'no subscription record was found!']);
    }
}
