<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MockController extends Controller
{
    public function mock(Request $request)
    {
        $receipt = $request->get('receipt');
        // mock limit error
        if (
            is_numeric($receipt[strlen($receipt) - 1]) &&
            $receipt[strlen($receipt) - 1] % 6 == 0 &&
            boolval(rand(0, 1))
        ) {
            return response(
                [
                    'error' => 'to many requests'
                ],
                Response::HTTP_TOO_MANY_REQUESTS);
        }

        return response(
            [
                'receipt' => $receipt,
                'expired_at' => Carbon::now()->addDays(rand(30, 80)),
            ]
        );
    }
}
