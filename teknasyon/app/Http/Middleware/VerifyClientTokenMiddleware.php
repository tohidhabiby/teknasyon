<?php

namespace App\Http\Middleware;

use App\Models\Device;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VerifyClientTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $device = $request->hasHeader('Client-Token') ?
            Device::whereClientToken($request->header('Client-Token'))->first() :
            null;

        // if client_token is not present in header or not device associated to client_token not found.
        if (empty($device)) {
            return response(
                [
                    'message' => 'you are not allowed to perform this action!',
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        // bind device in app so can be access in rest of request.
        app()->bind('device', fn() => $device);

        return $next($request);
    }
}
