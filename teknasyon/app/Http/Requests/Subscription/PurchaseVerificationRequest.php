<?php

namespace App\Http\Requests\Subscription;

use App\Http\Requests\BaseRequest;
use App\Models\Subscription;

class PurchaseVerificationRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Subscription::EXPIRED_AT => ['required'],
        ];
    }
}
