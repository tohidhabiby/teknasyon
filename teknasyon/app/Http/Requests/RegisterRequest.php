<?php

namespace App\Http\Requests;

use App\Models\Device;
use Illuminate\Validation\Rule;

class RegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        //todo: add rule to check (uid, app_id, os, username)
        return [
            Device::UID => ['required'],
            Device::APP_ID => ['required'],
            Device::OS => ['required', Rule::in(Device::$osTypes)],
            Device::LANGUAGE => ['required', 'string', 'max:2', 'min:2'],
            Device::USERNAME => ['required'],
            Device::PASSWORD => ['required'],
        ];
    }
}
