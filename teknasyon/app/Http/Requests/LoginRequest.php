<?php

namespace App\Http\Requests\Authentication;

use App\Http\Requests\BaseRequest;
use App\Models\Device;

class LoginRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Device::USERNAME => 'string',
            Device::PASSWORD => 'required|string',
        ];
    }
}
