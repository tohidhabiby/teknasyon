<?php

namespace App\Traits;

use App\Models\Device;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasDeviceIdTrait
{
    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
}
