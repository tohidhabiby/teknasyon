<?php

namespace App\Traits;

use App\Models\Device;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait ExternalServiceMockTrait
{
    /**
     * @param string $os OS.
     * @param string $receipt Receipt.
     * @param array $credentials Credentials.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function verifySubscription(string $os, string $receipt, array $credentials): mixed
    {
        // prepare request object
        $header = ['credential' => implode(':', $credentials)];
        $url = $os == Device::ANDROID ? '/api/mock/google-service' : '/api/mock/apple-service';
        $request = Request::create($url, 'POST', ['receipt' => $receipt]);
        $request->headers->add($header);

        return $os == Device::ANDROID ? $this->google($request) : $this->apple($request);
    }

    /**
     * @param Request $request Request.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function apple(Request $request): mixed
    {
        $response = app()->handle($request);

        if (!$response->isOk()) {
            return false;
        }

        $response = json_decode($response, true);
        $response[Subscription::EXPIRED_AT] = Carbon::parse($response[Subscription::EXPIRED_AT])->addHours(6);

        return $response;
    }

    /**
     * @param Request $request Request.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function google(Request $request): mixed
    {
        $response = app()->handle($request);

        if (!$response->isOk()) {
            return false;
        }

        return json_decode($response, true);
    }
}
