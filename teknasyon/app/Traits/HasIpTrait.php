<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasIpTrait
{
    /**
     * @param Builder     $builder Builder.
     * @param string|null $ip      IP.
     *
     * @return Builder
     */
    public function scopeWhereIpLike(Builder $builder, ?string $ip = null): Builder
    {
        return $builder->where(self::IP, 'LIKE', "%$ip%");
    }
}
