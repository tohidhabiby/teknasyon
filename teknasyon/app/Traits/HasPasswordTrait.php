<?php

namespace App\Traits;

use Illuminate\Support\Facades\Hash;

trait HasPasswordTrait
{
    /**
     * @param string $password
     * @return boolean
     */
    public function updatePassword(string $password): bool
    {
        $this->setPassword(Hash::make($password));
        return $this->save();
    }
}
