<?php

use App\Http\Controllers\MockController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SubscriptionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);


Route::middleware('verify.client-token')->prefix('subscriptions')->name('subscription.')
    ->group(function () {
        // subscription routes
        Route::post('purchase', [SubscriptionController::class, 'purchase'])->name('purchase');
        Route::post('status', [SubscriptionController::class, 'status'])->name('verification');
    });

Route::prefix('mock')->name('mock.')->group(function (){
    // mock routes
    Route::post('apple-service', [MockController::class, 'mock'])->name('apple.verification');
    Route::post('google-service', [MockController::class, 'mock'])->name('google.verification');
});
